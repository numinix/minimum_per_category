<?php

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible)
VALUES (NULL, 'Minimum Quantity Per Category', 'Set the minimum quantity that needs to be ordered in order to checkout on a per category basis', '1', '1')");
$configuration_group_id = $db->Insert_ID();

$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES
('Version', 'MINIMUM_PER_CATEGORY_VERSION', '1.2.0', 'Version Installed:', " . $configuration_group_id . ", 0, NOW()),

('Minimum quantity per category', 'MIN_QTY_CATEGORY','','Set the minimum weight that needs to be ordered in order to checkout on a group per category basis.<br/>For example: {234,187}:3;{152}:50 would mean group 1 (categories 234 & 187) require a minimum  weight of 3, and category 152 requires a minimum weight of 50 to checkout. ', ".$configuration_group_id.", 1, now());");


$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configMinPerCat' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configMinPerCat')) {
    $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MINIMUM_PER_CATEGORY_VERSION' LIMIT 1;");
    $configuration_group_id = $configuration->fields['configuration_group_id'];
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configMinPerCat',
                              'BOX_CONFIGURATION_MIN_PER_CAT', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Minimum Per Category Configuration menu.', 'success');
    }
  } 
}

$messageStack->add('Installed Minimum Per Category Checkout v1.1.0', 'success');
