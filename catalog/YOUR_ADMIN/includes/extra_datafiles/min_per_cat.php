<?php
/**
 * fec.php
 *
 * @package Fast and Easy Checkout
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: fec.php 2 2008-07-05 01:19:41Z numinix $
 */

define('FILENAME_MIN_PER_CAT', 'min_per_cat');
define('BOX_CONFIGURATION_MIN_PER_CAT', 'Minimum Per Category Configuration');