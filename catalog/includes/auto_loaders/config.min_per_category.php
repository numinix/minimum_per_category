<?php
$autoLoadConfig[80][] = [
  'autoType' => 'class',
  'loadFile' => 'min_per_category.php'
];

$autoLoadConfig[90][] = [
  'autoType' => 'class',
  'loadFile' => 'observers/class.min_per_category.php'
];
$autoLoadConfig[90][] = [
  'autoType' => 'classInstantiate',
  'className' => 'MinPerCategoryObserver',
  'objectName' => 'MinPerCategoryObserver'
];
