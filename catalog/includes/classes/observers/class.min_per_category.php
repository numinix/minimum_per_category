<?php
class MinPerCategoryObserver extends base
{
  function __construct()
  {
    global $zco_notifier;
    $zco_notifier->attach($this, ['NOTIFY_HEADER_START_CHECKOUT_PROCESS']);
    $zco_notifier->attach($this, ['NOTIFY_HEADER_START_CHECKOUT']);
  }

  function update(&$class, $eventID, $paramsArray)
  {
    $mpc = new MinPerCategory();
    $mpc->update();
  }
}
