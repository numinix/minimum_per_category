<?php

class MinPerCategory
{
    public $min_per_category = [];

    public function getAdminSettings()
    {
        if (!defined('MIN_QTY_CATEGORY')) {
            return [];
        }

        $result = [];
        $temp = MIN_QTY_CATEGORY;

        if (!preg_match('/{/', $temp)) {
            $items = explode(",", $temp);
            foreach ($items as $value) {
                $temp = explode(":", $value);
                $config_id = $temp[0];
                $minQty = $temp[1];
                $result[$config_id] = $minQty;
            }
        } else {
            $items = explode(";", $temp);
            $group_array = [];
            foreach ($items as $value) {
                $group_temp = explode(":", $value);
                $group_temp[0] = str_replace('{', '', $group_temp[0]);
                $group_temp[0] = str_replace('}', '', $group_temp[0]);
                $temp = explode(",", $group_temp[0]);
                $group_array[] = array($temp, $group_temp[1]);
            }
            $result = $group_array;
        }

        return $result;
    }

    public function getCategoryName($cID)
    {
        global $db;
        $query = "SELECT categories_name FROM " . TABLE_CATEGORIES_DESCRIPTION .
          " WHERE categories_id = '" . $cID . "' LIMIT 1";
        $queryResult = $db->Execute($query);
        return $queryResult->fields['categories_name'];
    }

    public function getProductData($pID, $columns = [])
    {
        global $db;

        array_push($columns, "p.products_id");
        $q_columns = implode(",", $columns);

        $queryResult = $db->Execute("SELECT $q_columns
                                      FROM " . TABLE_PRODUCTS . " p
                                      LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (p.products_id = pd.products_id)
                                      WHERE p.products_id = " . (int)$pID . "
                                      AND p.products_id = pd.products_id
                                      AND pd.language_id = " . (int)$_SESSION['languages_id']);

        array_pop($columns);
        $result = [];

        foreach ($columns as $column) {
            $result[$column] = $queryResult->fields[$column];
        }
        return $result;
    }
    //called by the observer
    public function update()
    {
        $_SESSION['debug'] = 'start' . "\n";
        $temp = MIN_QTY_CATEGORY;
        $_SESSION['debug'] .= 'Admin Parameters' . $temp . "\n";

        if (!preg_match('/{/', $temp)) { // Non Grouped
            $_SESSION['debug'] .= 'no Group';
            $adminSettings = $this->getAdminSettings();
            $totalProductsFromCategory = array();
            foreach ($adminSettings as $key => $value) {
                $totalProductsFromCategory[$key] = 0;
            }

            $cartProducts = $_SESSION['cart']->get_products();
            $categoriesOrderedFrom = array();
            foreach ($cartProducts as $product) {
                $prod_id = $product['id'];
                $prodQty = $product['quantity'];
                foreach ($adminSettings as $key => $value) {
                    if (zen_product_in_category($prod_id, $key)) {
                        $totalProductsFromCategory[$key] += $prodQty;
                        $categoriesOrderedFrom[] = $key;
                    }
                }
            }
            $error = false;
            $errorMessages = array();
            global $messageStack;
            foreach ($totalProductsFromCategory as $key => $value) {
                if (in_array($key, $categoriesOrderedFrom) && $adminSettings[$key] > $value) {
                    $error = true;
                    $errorMessages[] = "<p>A minimum order of " . $adminSettings[$key] . " from the category '"
                      . $this->getCategoryName($key) . "' is required to checkout.</p>";
                }
            }
            if ($error) {
                $messageStack->add_session('shopping_cart', implode('<br/>', $errorMessages), 'error');
                zen_redirect(zen_href_link(FILENAME_SHOPPING_CART));
            }
        } else { // If group
            $_SESSION['debug'] .= 'group' . "\n";
            $adminSettings = $this->getAdminSettings();
            $totalProductsFromGroup = [];
            foreach ($adminSettings as $key => $value) {
                $totalProductsFromGroup[$key] = 0;
            }

            $cartProducts = $_SESSION['cart']->get_products();
            $categoriesOrderedFrom = [];
            foreach ($cartProducts as $product) {
                $prod_id = $product['id'];
                $pId = explode(":", $product['id'])[0];
                $pdata = $this->getProductData($pId, ['products_weight']);
                $prodQty = $product['quantity'] * $pdata['products_weight'];

                foreach ($adminSettings as $key => $value) {
                    $products_present = false;
                    foreach ($value[0] as $category) {
                        $_SESSION['debug'] .= $category . "\n";
                        if (zen_product_in_category($prod_id, $category)) {
                            $totalProductsFromGroup[$key] += $prodQty;
                            $categoriesOrderedFrom[] = $category;
                            $prodQty = 0;
                            $products_present = true;
                        }
                        $categoriesInGroup[$key][] = $category;
                    }
                    if ($products_present == true) {
                        $totalProductsNeededFromGroup[$key] = $value[1];
                    }
                    $_SESSION['debug'] .= '-' . $key . ':' . $totalProductsFromGroup[$key] . "needs-" . $totalProductsNeededFromGroup[$key] . " ";
                }
            }
            $error = false;
            $errorMessages = [];
            global $messageStack;
            
            foreach ($totalProductsFromGroup as $key => $value) {
                $_SESSION['debug'] .= $key . "= " . $totalProductsNeededFromGroup[$key] . " > " . $value;
                if ($totalProductsNeededFromGroup[$key] > $value && $value != 0) {
                    $error = true;
                    $uniqueCategories = array_unique($categoriesInGroup[$key]); // Get unique category IDs directly from the group.
                    $categoriesig_text = '<br/>';
                    foreach ($uniqueCategories as $catId) {
                        $categoriesig_text .= '<a href="' . zen_href_link(FILENAME_DEFAULT, 'cPath=' . $catId) . '">'
                          . $this->getCategoryName($catId) . '</a><br/>';
                    }
                    $errorMessages[] = "<p>A minimum order of " . $adminSettings[$key][1]
                      . " from these categories " . $categoriesig_text . " is required to checkout.</p>";
                }
            }            

            if ($error) {
                $messageStack->add_session('shopping_cart', implode('<br/>', $errorMessages), 'error');
                zen_redirect(zen_href_link(FILENAME_SHOPPING_CART));
            }
        }
    }
}
